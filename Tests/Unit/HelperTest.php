<?php

namespace TaylorThomas\TimberLazysizesUtils\Tests\Unit;

use TaylorThomas\TimberLazysizesUtils\Helper;

class HelperTest extends TestCase
{

    const SIZES = [
        'large' => [
            'file'      => '1024w.jpg',
            'width'     => 1024,
            'height'    => 768,
            'mime-type' => 'image/jpeg'
        ],
        'image-512w' => [
            'file'      => '512w.jpg',
            'width'     => 512,
            'height'    => 384,
            'mime-type' => 'image/jpeg'
        ],
        'image-1280w' => [
            'file'      => '1280w.jpg',
            'width'     => 1280,
            'height'    => 960,
            'mime-type' => 'image/jpeg'
        ],
        'image-1536w' => [
            'file'      => '1536w.jpg',
            'width'     => 1536,
            'height'    => 1152,
            'mime-type' => 'image/jpeg'
        ],
        'image-1792w' => [
            'file'      => '1792w.jpg',
            'width'     => 1792,
            'height'    => 1344,
            'mime-type' => 'image/jpeg'
        ],
        'image-2048w' => [
            'file'      => '2048w.jpg',
            'width'     => 2048,
            'height'    => 1536,
            'mime-type' => 'image/jpeg'
        ]
    ];


    const SRCSET = 'http://www.example.com/512w.jpg 512w, http://www.example.com/1024w.jpg 1024w, http://www.example.com/1280w.jpg 1280w, http://www.example.com/1536w.jpg 1536w, http://www.example.com/1792w.jpg 1792w, http://www.example.com/2048w.jpg 2048w 1536h';


    protected function getImage() : \Timber\Image
    {
        $image = \Mockery::mock('\Timber\Image');
        $image->caption = 'An image';
        $image->sizes = self::SIZES;
        $image->src = 'http://www.example.com/original-image.jpg';
        return $image;
    }


    public function testBackgroundAttributesFor()
    {
        $expected = 'data-bgset="' . self::SRCSET .'" data-parent-fit="cover" data-sizes="auto" data-caption="An image"';
        $actual = Helper::backgroundAttributesFor($this->getImage());
        $this->assertEquals($expected, $actual);
    }


    public function testInlineAttributesFor()
    {
        $expected = 'data-srcset="' . self::SRCSET .'" data-sizes="auto" data-caption="An image"';
        $actual = Helper::inlineAttributesFor($this->getImage());
        $this->assertEquals($expected, $actual);
    }


    public function testBackgroundDataFor()
    {
        $expected = [
            'bgset'      => self::SRCSET,
            'parent-fit' => 'cover',
            'sizes'      => 'auto',
            'caption'    => 'An image'
        ];
        $actual = Helper::backgroundDataFor($this->getImage());
        $this->assertEquals($expected, $actual);
    }


    public function testInlineDataFor()
    {
        $expected = [
            'srcset'     => self::SRCSET,
            'sizes'      => 'auto',
            'caption'    => 'An image'
        ];
        $actual = Helper::inlineDataFor($this->getImage());
        $this->assertEquals($expected, $actual);
    }
}
