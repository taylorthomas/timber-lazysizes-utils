<?php

namespace TaylorThomas\TimberLazysizesUtils;

abstract class AbstractDataBuilder implements DataBuilderInterface
{
    protected $caption;
    protected $srcsetBuilder;

    public function __construct(SrcsetBuilder $srcsetBuilder, ?string $caption)
    {
        $this->srcsetBuilder = $srcsetBuilder;
        $this->caption       = $caption;
    }
}
