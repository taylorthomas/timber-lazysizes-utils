<?php

namespace TaylorThomas\TimberLazysizesUtils;

class BackgroundDataBuilder extends AbstractDataBuilder
{
    public function build() :Array
    {
        return [
            'bgset'      => $this->srcsetBuilder->build(),
            'parent-fit' => 'cover',
            'sizes'      => 'auto',
            'caption'    => $this->caption
        ];
    }
}
