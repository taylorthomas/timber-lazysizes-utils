<?php

namespace TaylorThomas\TimberLazysizesUtils;

interface DataBuilderInterface
{
  public function build() : Array;
}
