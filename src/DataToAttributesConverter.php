<?php

namespace TaylorThomas\TimberLazysizesUtils;

class DataToAttributesConverter
{
    protected $dataBuilder;

    public function __construct(DataBuilderInterface $dataBuilder)
    {
        $this->dataBuilder = $dataBuilder;
    }

    public function convert()
    {
        $data = $this->dataBuilder->build();

        $attributeStrings = array_map(function ($value, $key) {
            return "data-{$key}=\"{$value}\"";
        }, $data, array_keys($data));

        return implode(' ', $attributeStrings);
    }
}
