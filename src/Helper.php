<?php

namespace TaylorThomas\TimberLazysizesUtils;

class Helper
{
    const BACKGROUND_DATA_BUILDER = 'TaylorThomas\TimberLazysizesUtils\BackgroundDataBuilder';
    const INLINE_DATA_BUILDER     = 'TaylorThomas\TimberLazysizesUtils\InlineDataBuilder';


    protected static function getDataBuilder(\Timber\Image $image, $klass)
    {
        $srcsetBuilder = new SrcsetBuilder($image);
        return new $klass($srcsetBuilder, $image->caption);
    }

    protected static function attributesFor(\Timber\Image $image, $klass)
    {
      $dataBuilder = self::getDataBuilder($image, $klass);
      $dataToAttributeConverter = new DataToAttributesConverter($dataBuilder);
      return $dataToAttributeConverter->convert();
    }

    protected static function dataFor(\Timber\Image $image, $klass)
    {
      $dataBuilder = self::getDataBuilder($image, $klass);
      return $dataBuilder->build();
    }

    public static function backgroundAttributesFor(\Timber\Image $image)
    {
      return self::attributesFor($image, self::BACKGROUND_DATA_BUILDER);
    }

    public static function inlineAttributesFor(\Timber\Image $image)
    {
      return self::attributesFor($image, self::INLINE_DATA_BUILDER);
    }

    public static function backgroundDataFor(\Timber\Image $image)
    {
      return self::dataFor($image, self::BACKGROUND_DATA_BUILDER);
    }

    public static function inlineDataFor(\Timber\Image $image)
    {
      return self::dataFor($image, self::INLINE_DATA_BUILDER);
    }
}
