<?php

namespace TaylorThomas\TimberLazysizesUtils;

class InlineDataBuilder extends AbstractDataBuilder
{
    public function build() :Array
    {
        return [
            'srcset'  => $this->srcsetBuilder->build(),
            'sizes'   => 'auto',
            'caption' => $this->caption
        ];
    }
}
