<?php

namespace TaylorThomas\TimberLazysizesUtils;

class SrcsetBuilder
{

    protected $image;

    public function __construct(\Timber\Image $image)
    {
        $this->image = $image;
    }


    public function build()
    {
        $sizes = $this->image->sizes;
        $url = rtrim(preg_replace('/[^\/]+$/', '', $this->image->src), '/') . '/';

        $sizesCount = count($sizes);
        
        usort($sizes, function ($a, $b) {
            return $a['width'] <=> $b['width'];
        });

        $strings = array_map(function ($size, $index) use ($sizesCount, $url) {
            $string = "{$url}{$size['file']} {$size['width']}w";
            if ($index == $sizesCount - 1) {
                $string .= " {$size['height']}h";
            }
            return $string;
        }, $sizes, array_keys($sizes));
        return implode(', ', $strings);
    }
}
